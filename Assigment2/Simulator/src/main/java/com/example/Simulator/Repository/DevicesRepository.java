package com.example.Simulator.Repository;

import com.example.Simulator.entity.Devices;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DevicesRepository extends JpaRepository<Devices, Integer> {

}
