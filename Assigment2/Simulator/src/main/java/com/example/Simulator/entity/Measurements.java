package com.example.Simulator.entity;

import lombok.Data;

@Data
public class Measurements {

    private Integer device_id;
    private String timestamp;
    private String consumption;
    public Measurements(Integer device_id,String timestamp, String consumption) {
        this.device_id = device_id;
        this.timestamp = timestamp;
        this.consumption = consumption;
    }
}
