package com.example.Simulator;
import com.example.Simulator.entity.Measurements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.io.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Properties;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Component
@Service
public class Producer {


    @Value("${rabbitmq.exchange.name}")
    private String exchange;

    @Value("${rabbitmq.routing.json.key}")
    private String routingJsonKey;

    @Value("${rabbitmq.routing.json2.key}")
    private String routingJsonKey2;

    private RabbitTemplate rabbitTemplate;
    private Integer device_id;
    private DevicesService devicesService;
    private static final Logger LOGGER = LoggerFactory.getLogger(Producer.class);
    private static final String CSV_FILE_PATH = "C:/Users/alexi/Documents/SD2023/Assigment2/sensor.csv";
    private final SimpMessagingTemplate messagingTemplate;
    private BufferedReader br;
    private BufferedReader br2;
    private ScheduledExecutorService executorService;
    private ScheduledExecutorService executorService2;
    private boolean isInitialized = false;

    private boolean isInitialized2 = false;
    private Integer max_consumption;
    private Integer max_consumption2;

    Properties properties = readConfig();
    private Integer device_id1;
    private Integer device_id2;

    @Autowired
    public Producer(RabbitTemplate rabbitTemplate,
                    SimpMessagingTemplate messagingTemplate,
                    DevicesService devicesService) {
        this.rabbitTemplate = rabbitTemplate;
        this.messagingTemplate = messagingTemplate;
        this.devicesService = devicesService;
        this.device_id1 = Integer.parseInt(properties.getProperty("device_id1"));
        this.device_id2 = Integer.parseInt(properties.getProperty("device_id2"));
    }
    public void init() {
        if (!isInitialized) {
            try {
                br = new BufferedReader(new FileReader(CSV_FILE_PATH));
                scheduleTask();
                isInitialized = true;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void init2() {
        if (!isInitialized2) {
            try {
                br2 = new BufferedReader(new FileReader(CSV_FILE_PATH));
                scheduleTask2();
                isInitialized2 = true;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void scheduleTask() {
        executorService = Executors.newSingleThreadScheduledExecutor();
        executorService.scheduleAtFixedRate(this::readLineTask, 0, 1, TimeUnit.SECONDS);
    }

    private void scheduleTask2() {
        executorService2 = Executors.newSingleThreadScheduledExecutor();
        executorService2.scheduleAtFixedRate(this::readLineTask2, 0, 1, TimeUnit.SECONDS);
    }

    public void setDeviceId(Integer deviceId) {
        this.device_id = deviceId;
    }
    public void readLineTask() {
        System.out.println(device_id1);
        max_consumption = devicesService.getMaxConsumption(device_id1);
        System.out.println(max_consumption);
        try {
            String consumption = br.readLine();

            if (consumption == null) {
                System.out.println("End of file reached.");
                isInitialized = false;

            } else {
                System.out.println(consumption);
                LocalDateTime timeStamp = LocalDateTime.now();
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
                String formattedDateTime = timeStamp.format(formatter);
                System.out.println(formattedDateTime);

                Measurements measurements = new Measurements(device_id1, formattedDateTime, consumption);

                if (Double.parseDouble(consumption) > max_consumption) {
                    Integer userID = devicesService.getUserId(device_id1);
                    messagingTemplate.convertAndSend("/topic/notification", "High consumption detected! Device 9, User: "+ userID);
                }

                sendMessageToQueue(measurements);
            }
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void readLineTask2() {
        System.out.println(device_id2);
        max_consumption2 = devicesService.getMaxConsumption(device_id2);
        System.out.println(max_consumption2);
        try {
            String consumption2 = br2.readLine();

            if (consumption2 == null) {
                System.out.println("End of file reached.");
                isInitialized2 = false;

            } else {
                System.out.println(consumption2);
                LocalDateTime timeStamp = LocalDateTime.now();
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
                String formattedDateTime = timeStamp.format(formatter);
                System.out.println(formattedDateTime);

                Measurements measurements = new Measurements(device_id2, formattedDateTime, consumption2);

                if (Double.parseDouble(consumption2) > max_consumption2) {
                    Integer userID = devicesService.getUserId(device_id2);
                    messagingTemplate.convertAndSend("/topic/notification2", "High consumption detected! Device 16, User: "+ userID);
                }



                sendMessageToQueue1(measurements);
            }
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    public void sendMessageToQueue (Measurements measurements){
        LOGGER.info(String.format("Json message sent -> %s", measurements.toString()));
        rabbitTemplate.convertAndSend(exchange, routingJsonKey, measurements);
    }
    public void sendMessageToQueue1 (Measurements measurements){
        LOGGER.info(String.format("Json message from 2nd queue sent -> %s", measurements.toString()));
        rabbitTemplate.convertAndSend(exchange, routingJsonKey2, measurements);
    }

    public void stopTask() {
        if (executorService != null && !executorService.isShutdown()) {
            executorService.shutdown();
        }
    }

    public void stopTask2() {
        if (executorService2 != null && !executorService2.isShutdown()) {
            executorService2.shutdown();
        }
    }
    private static Properties readConfig() {
        Properties properties = new Properties();
        try (FileInputStream input = new FileInputStream("config.properties")) {
            properties.load(input);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return properties;
    }
    public void resetState() {
        isInitialized = false;
        stopTask();
        init();
    }

    public void resetState2() {
        isInitialized2 = false;
        stopTask();
        init2();
    }
}