package com.example.Simulator;

import com.example.Simulator.Repository.DevicesRepository;
import com.example.Simulator.entity.Devices;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class DevicesService {

    private final DevicesRepository devicesRepository;

    public DevicesService(DevicesRepository devicesRepository){
        this.devicesRepository = devicesRepository;
    }
    public Integer getMaxConsumption(Integer deviceId) {
        Optional<Devices> device =  devicesRepository.findById(deviceId);
        return device.map(Devices::getMax_consumption).orElse(null);
    }

    public Integer getUserId(Integer deviceId){
        Optional<Devices> device =  devicesRepository.findById(deviceId);
        return device.map(Devices::getUser_id).orElse(null);
    }

}
