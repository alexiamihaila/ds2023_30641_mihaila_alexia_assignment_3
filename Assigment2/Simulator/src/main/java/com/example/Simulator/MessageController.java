package com.example.Simulator;

import jakarta.annotation.PreDestroy;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Map;
import java.util.Properties;

@RestController
@RequestMapping("/api/simulator")
public class MessageController {
    private final Producer jsonProducer;
    //Properties properties = readConfig();
    //String device_id1 = properties.getProperty("device_id1");
    //String device_id2 = properties.getProperty("device_id2");

    public MessageController(Producer producer){
        this.jsonProducer = producer;
    }

    @PostMapping("/publish")
    public ResponseEntity<String> sendMessage(/*@RequestBody Map<String, Object> payload*/){

        jsonProducer.resetState();
        jsonProducer.init();

        return ResponseEntity.ok("Message  sent to RabbitMQ...");
    }


    @PostMapping("/publish2")
    public ResponseEntity<String> sendMessage2(/*@RequestBody Map<String, Object> payload*/){

        jsonProducer.resetState2();
        jsonProducer.init2();

        return ResponseEntity.ok("Message 2 sent to RabbitMQ...");
    }


    @PreDestroy
    public void stopProducerTask() {
        jsonProducer.stopTask();
    }


    @PreDestroy
    public void stopProducerTask2() {
        jsonProducer.stopTask2();
    }
}
