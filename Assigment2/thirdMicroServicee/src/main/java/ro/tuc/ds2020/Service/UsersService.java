package ro.tuc.ds2020.Service;

import org.springframework.stereotype.Service;
import ro.tuc.ds2020.Entity.Users;
import ro.tuc.ds2020.Repository.UsersRepository;

@Service
public class UsersService {

    private UsersRepository usersRepository;

    public UsersService(UsersRepository usersRepository){
        this.usersRepository = usersRepository;
    }

    public void saveUser(Users user){
        usersRepository.save(user);
    }

    public void deleteUser(Integer id){
        usersRepository.deleteById(id);
    }
}
