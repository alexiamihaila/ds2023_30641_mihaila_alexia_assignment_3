package ro.tuc.ds2020.DTO;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class DevicesDto {
    @JsonProperty("id_device")
    private Integer id_device;
    @JsonProperty("max_consumption")
    private Integer max_consumption;
    @JsonProperty("user_id")
    private Integer user_id;

    @JsonProperty("operation")
    private String operation;

    @JsonCreator()
    public DevicesDto( @JsonProperty("id_device") Integer id_device,
                       @JsonProperty("max_consumption") Integer max_consumption,
                       @JsonProperty("user_id")Integer user_id,
                       @JsonProperty("operation") String operation) {
        this.id_device = id_device;
        this.max_consumption = max_consumption;
        this.user_id = user_id;
        this.operation = operation;
    }

    public DevicesDto(){}

}

