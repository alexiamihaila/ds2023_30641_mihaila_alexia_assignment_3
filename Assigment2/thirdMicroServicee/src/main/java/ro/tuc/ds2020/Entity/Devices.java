package ro.tuc.ds2020.Entity;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Data;

@Data
@Entity
@Table(name = "devices")
public class Devices {
    @Id
    private Integer id_device;

    @JsonProperty("max_consumption")
    @Column(name = "max_consumption")
    private Integer max_consumption;


    @JsonProperty("user_id")
    @Column(name = "user_id")
    private Integer user_id;


    @JsonCreator()
    public Devices(Integer id_device, Integer max_consumption, Integer user_id) {
        this.id_device = id_device;
        this.max_consumption = max_consumption;
        this.user_id = user_id;
    }


    public Devices(){}

}
