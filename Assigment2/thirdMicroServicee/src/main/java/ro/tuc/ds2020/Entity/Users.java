package ro.tuc.ds2020.Entity;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Data;

@Data
@Entity
@Table(name = "users")
public class Users {

    @Id
    //@JsonProperty("user_id")
    private Integer user_id;

    @JsonCreator
    public Users(@JsonProperty("user_id") Integer user_id){
        this.user_id = user_id;
    }
    public Users(){}
}
