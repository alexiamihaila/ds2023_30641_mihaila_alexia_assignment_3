package ro.tuc.ds2020.Service;

import org.springframework.stereotype.Service;
import ro.tuc.ds2020.Entity.Measurements;
import ro.tuc.ds2020.Repository.MeasurementsRepository;

import java.util.List;

@Service
public class MeasurementsService {

    private MeasurementsRepository measurementsRepository;

    public MeasurementsService(MeasurementsRepository measurementsRepository) {
        this.measurementsRepository = measurementsRepository;
    }

    public void saveMeasurements(Measurements measurements) {
        measurementsRepository.save(measurements);
    }


    public List<Measurements> getMeasurementsForDateAndDeviceId(String date, Integer deviceId) {
        return measurementsRepository.findByDeviceIdAndTimestampContaining(deviceId,date);
    }
}