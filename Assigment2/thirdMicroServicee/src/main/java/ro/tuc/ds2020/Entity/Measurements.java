package ro.tuc.ds2020.Entity;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.persistence.*;
import lombok.Data;

@Data
@Entity
@Table(name = "measurements")
public class Measurements {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id_measurement;

    @JsonProperty("device_id")
    @Column(name = "id_device")
    private Integer deviceId;

    @JsonProperty("timestamp")
    @Column(name = "time_stamp")
    private String timestamp;

    @JsonProperty("consumption")
    @Column(name = "consumption")
    private String consumption;

    @JsonCreator
    public Measurements(
            @JsonProperty("device_id") Integer device_id,
            @JsonProperty("timestamp") String timestamp,
            @JsonProperty("consumption") String consumption) {
        this.deviceId = device_id;
        this.timestamp = timestamp;
        this.consumption = consumption;
    }

    public Measurements() {

    }
}
