package ro.tuc.ds2020.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ro.tuc.ds2020.Entity.Devices;

@Repository
public interface DevicesRepository extends JpaRepository<Devices, Integer> {

}
