package ro.tuc.ds2020.DTO;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class UserDto {
    @JsonProperty("user_id")
    private Integer id_user;
    @JsonProperty("operation")
    private String operation;

    @JsonCreator
    public UserDto(@JsonProperty("user_id") Integer id_user
            , @JsonProperty("operation") String operation){
        this.id_user = id_user;
        this.operation = operation;
    }
}
