package ro.tuc.ds2020;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.DTO.DevicesDto;
import ro.tuc.ds2020.Entity.Devices;
import ro.tuc.ds2020.Entity.Measurements;
import ro.tuc.ds2020.Entity.Users;
import ro.tuc.ds2020.Service.DevicesService;
import ro.tuc.ds2020.Service.MeasurementsService;
import ro.tuc.ds2020.Service.UsersService;

@Service
public class RabbitMQConsumer {

    private static final Logger LOGGER = LoggerFactory.getLogger(RabbitMQConsumer.class);
    private final MeasurementsService measurementsService;
    private final UsersService usersService;
    private final DevicesService devicesService;

    public RabbitMQConsumer(MeasurementsService measurementsService, UsersService usersService,DevicesService devicesService){
        this.measurementsService = measurementsService;
        this.usersService = usersService;
        this.devicesService = devicesService;
    }

    @RabbitListener(queues ={"${rabbitmq.queue.json.name}"})
    public void consume(byte[] message){
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            Measurements receivedMessage = objectMapper.readValue(message, Measurements.class);

            LOGGER.info(String.format("Received message -> %s", receivedMessage));
            measurementsService.saveMeasurements(receivedMessage);

        } catch (Exception e) {
            LOGGER.error("Error deserializing message", e);
        }
    }


    @RabbitListener(queues ={"${rabbitmq.queue.json2.name}"})
    public void consume2(byte[] message){
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            Measurements receivedMessage = objectMapper.readValue(message, Measurements.class);

            LOGGER.info(String.format("Received message in 2nd queue -> %s", receivedMessage));
            measurementsService.saveMeasurements(receivedMessage);

        } catch (Exception e) {
            LOGGER.error("Error deserializing message", e);
        }
    }

    @RabbitListener(queues ={"${rabbitmq.queue.json3.name}"})
    public void addUser(byte[] message){

        try {
            ObjectMapper objectMapper = new ObjectMapper();
            //UserDto receivedMessage = objectMapper.readValue(message, UserDto.class);
            DevicesDto receivedMessage = objectMapper.readValue(message,DevicesDto.class);
            if(receivedMessage.getOperation().equals("addUser")) {
                Users user = new Users(receivedMessage.getUser_id());
                usersService.saveUser(user);

            }
            else if(receivedMessage.getOperation().equals("deleteUser")) {
                usersService.deleteUser(receivedMessage.getUser_id());

            }
            else if(receivedMessage.getOperation().equals("addDevice")){
                Devices devices = new Devices(receivedMessage.getId_device(),
                        receivedMessage.getMax_consumption(),
                        receivedMessage.getUser_id());
                devicesService.saveDevice(devices);
            }
            else if(receivedMessage.getOperation().equals("deleteDevice")){
                devicesService.deleteDevice(receivedMessage.getId_device());
            }

            LOGGER.info(String.format(receivedMessage.getOperation() +"with id -> %s", receivedMessage));


        } catch (Exception e) {
            LOGGER.error("Error deserializing message", e);
        }
    }
}
