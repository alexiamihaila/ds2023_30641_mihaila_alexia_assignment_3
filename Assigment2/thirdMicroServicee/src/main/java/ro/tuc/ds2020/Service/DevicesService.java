package ro.tuc.ds2020.Service;

import org.springframework.stereotype.Service;
import ro.tuc.ds2020.Entity.Devices;
import ro.tuc.ds2020.Repository.DevicesRepository;

@Service
public class DevicesService {
    private DevicesRepository devicesRepository;

    public DevicesService(DevicesRepository devicesRepository){
        this.devicesRepository = devicesRepository;
    }

    public void saveDevice(Devices device){
        devicesRepository.save(device);
    }

    public void deleteDevice(Integer id){
        devicesRepository.deleteById(id);
    }

}
