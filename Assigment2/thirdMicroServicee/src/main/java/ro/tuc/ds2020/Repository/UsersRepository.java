package ro.tuc.ds2020.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ro.tuc.ds2020.Entity.Users;

@Repository
public interface UsersRepository extends JpaRepository<Users, Integer> {
}
