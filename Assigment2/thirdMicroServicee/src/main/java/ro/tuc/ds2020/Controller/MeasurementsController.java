package ro.tuc.ds2020.Controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ro.tuc.ds2020.Entity.Measurements;
import ro.tuc.ds2020.Service.MeasurementsService;

import java.util.List;

@RestController
@RequestMapping("/api/consumption")
public class MeasurementsController {

    private final MeasurementsService measurementsService;

    public MeasurementsController(MeasurementsService measurementsService){
        this.measurementsService = measurementsService;
    }

    @GetMapping("/history")
    public ResponseEntity<List<Measurements>> getConsumptionHistory(
            @RequestParam("device_id") int deviceId,
            @RequestParam("date") String date) {

        List<Measurements> history = measurementsService.getMeasurementsForDateAndDeviceId( date, deviceId);
        return ResponseEntity.ok(history);
    }
}