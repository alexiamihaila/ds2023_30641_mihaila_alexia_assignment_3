package ro.tuc.ds2020.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ro.tuc.ds2020.Entity.Measurements;

import java.util.List;

public interface MeasurementsRepository extends JpaRepository<Measurements,Integer> {
    List<Measurements> findByDeviceIdAndTimestampContaining(Integer deviceId, String date);

}
