package utcn.ds.users.repositories;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import utcn.ds.users.entities.User;


@Repository
public interface UserRepository extends JpaRepository<User,Integer> {
    public void deleteUserByUsername(String username);
    public boolean existsUserByUsername(String username);
    public User findUserByUsername(String username);
}
