package utcn.ds.users.entities;

public enum Role {
    ADMIN,
    CLIENT
}
