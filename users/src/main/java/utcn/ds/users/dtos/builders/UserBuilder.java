package utcn.ds.users.dtos.builders;


import utcn.ds.users.dtos.UserDTO;
import utcn.ds.users.entities.User;

public class UserBuilder {
    private UserBuilder(){

    }

    public static UserDTO toUserDTO(User user){
        return new UserDTO(user.getId_user(),user.getUsername(), user.getRole(), user.getPassword());
    }

    public static User toEntity(Integer user_id, UserDTO userDTO){
        return new User(user_id, userDTO.getUsername(),userDTO.getRole(),userDTO.getPassword());
    }
}
