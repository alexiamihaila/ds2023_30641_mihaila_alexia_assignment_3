package utcn.ds.users.dtos;

import lombok.Data;
import utcn.ds.users.entities.Role;


@Data
public class UserDeviceDTO {
    private String username;
    private Role role;

    public UserDeviceDTO(String username,  Role role){
        this.username = username;
        this.role = role;
    }
}
