package ro.tuc.ds2020.model;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class Message {
    private String messageId;
    private String senderName;
    private String receiverName;
    private String message;
    private String date;
    private Status status;
    private boolean read;
}
