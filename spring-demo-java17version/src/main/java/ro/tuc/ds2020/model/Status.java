package ro.tuc.ds2020.model;

public enum Status {
    JOIN,
    MESSAGE,
    READ,
    TYPING
}
