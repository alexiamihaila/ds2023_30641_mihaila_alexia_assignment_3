package ro.tuc.ds2020.model;

public enum Role {
    ADMIN,
    CLIENT
}
